import os

from django.apps import apps
from django.conf import settings
from django.urls import re_path
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sitemaps import views as sitemaps_views
from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import include, path
from django.views.generic.base import RedirectView

sitemaps = {}

urlpatterns = [
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("admin/", admin.site.urls),
    re_path("^i18n/", include("django.conf.urls.i18n")),
    re_path(r"^watchman/", include("watchman.urls")),
    re_path(r"^robots\.txt", include("robots.urls")),
    re_path(
        r"^security\.txt",
        include("django_mdat_security_txt.django_mdat_security_txt.urls"),
    ),
    re_path(
        r"^\.well-known/security\.txt",
        include("django_mdat_security_txt.django_mdat_security_txt.urls"),
    ),
    path("", include(apps.get_app_config("oscar").urls[0])),
    path("api/", include("oscarapi.urls")),
]

# Dynamic loading of Favicon urls
for name in os.listdir(
    settings.PROJECT_ROOT
    + "/../django_customization_dolphinit/django_customization_dolphinit/static/company/favicon"
):
    urlpatterns.append(
        re_path(
            name,
            RedirectView.as_view(
                url=staticfiles_storage.re_path("company/favicon/" + name),
                permanent=False,
            ),
        )
    )

urlpatterns += i18n_patterns(
    path("sitemap.xml", sitemaps_views.index, {"sitemaps": sitemaps}),
    path(
        "sitemap-<section>.xml",
        sitemaps_views.sitemap,
        {"sitemaps": sitemaps},
        name="django.contrib.sitemaps.views.sitemap",
    ),
    # re_path(r'', include('fluent_pages.urls')),
    re_path(
        r"", include("django_ecommerce_shop_general.django_ecommerce_shop_general.urls")
    ),
    path("mdat/location/", include("django_mdat_location.django_mdat_location.urls")),
)

# List modules that should not auto-map path
ignore_modules = [
    "django_ecommerce_shop_general",
    "django_ecommerce_shop_main",
    "django_ecommerce_shop_tasks",
]

# Dynamic loading of CMS urls
for name in os.listdir(settings.PROJECT_ROOT + "/.."):
    if os.path.isdir(name) and name.startswith("django_ecommerce_shop_"):
        if name in ignore_modules:
            continue

        module_name = name + "." + name
        url_path = name.replace("django_ecommerce_shop_", "").replace("_", "-")

        urlpatterns += i18n_patterns(
            path(url_path + "/", include(module_name + ".urls")),
        )

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
