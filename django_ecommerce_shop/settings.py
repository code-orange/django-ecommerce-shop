import os

from oscar.defaults import *

import sentry_sdk
from decouple import config
from django.utils.translation import gettext_lazy as _
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.django import DjangoIntegration

# FIX GDAL ON WINDOWS
if os.name == "nt":
    import platform

    OSGEO4W = r"C:\OSGeo4W"
    if "64" in platform.architecture()[0]:
        OSGEO4W += "64"
    assert os.path.isdir(OSGEO4W), "Directory does not exist: " + OSGEO4W
    os.environ["OSGEO4W_ROOT"] = OSGEO4W
    os.environ["GDAL_DATA"] = OSGEO4W + r"\share\gdal"
    os.environ["PROJ_LIB"] = OSGEO4W + r"\share\proj"
    os.environ["PATH"] = OSGEO4W + r"\bin;" + os.environ["PATH"]

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# All settings common to all environments
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
PROJECT_NAME = os.path.basename(PROJECT_ROOT)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config("SECRET_KEY", cast=str)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config("DEBUG", default=False, cast=bool)

SITE_ID = 1

SITEMAP_PREFIX = config("SITEMAP_PREFIX", default="https://example.com", cast=str)

PHONENUMBER_DEFAULT_REGION = "DE"

SESSION_ENGINE = "user_sessions.backends.db"

SILENCED_SYSTEM_CHECKS = ["captcha.recaptcha_test_key_error", "admin.E410"]

SESSION_COOKIE_SECURE = not DEBUG

CSRF_COOKIE_SECURE = not DEBUG

SESSION_COOKIE_HTTPONLY = True

CSRF_COOKIE_HTTPONLY = True

SESSION_COOKIE_NAME = "__Secure-sessionid" if not DEBUG else "sessionid"

CSRF_COOKIE_NAME = "__Secure-csrftoken" if not DEBUG else "csrftoken"

ALLOWED_HOSTS = ["*"]
INTERNAL_IPS = [
    "127.0.0.1",
]
CORS_ORIGIN_ALLOW_ALL = True

WATCHMAN_TOKENS = config("WATCHMAN_TOKENS", default=SECRET_KEY, cast=str)

APPEND_SLASH = True

TASTYPIE_ALLOW_MISSING_SLASH = True
TASTYPIE_ABSTRACT_APIKEY = True

# Application definition
INSTALLED_APPS = [
    # WhiteNoise - static file handling
    "whitenoise.runserver_nostatic",
    # file storage
    "db_file_storage",
    "easy_thumbnails",
    # editor
    "tinymce",
    "ckeditor",
    # Analytics / Tracking
    "analytical",
    # CORS headers
    "corsheaders",
    # Cookie consent
    "django_cookiebot.django_cookiebot",
    # Debug Toolbar
    "debug_toolbar",
    # Django monitoring
    "watchman",
    # Minify,
    "django_minify_html",
    # multi-language
    "parler",
    # page tools
    "mptt",
    "polymorphic",
    "polymorphic_tree",
    # django-autocomplete-light
    "dal",
    "dal_select2",
    # Django modules
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "user_sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "django.contrib.flatpages",
    "django.contrib.sitemaps",
    # Robots
    "robots",
    # LDAP Auth modules
    "django_python3_ldap",
    "django_session_ldap_attributes.django_session_ldap_attributes",
    # API
    "rest_framework",
    "tastypie",
    "django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth",
    "rpc4django",
    # Celery task
    "django_celery_results",
    "django_celery_beat",
    "flower",
    # Template
    "django_customization_dolphinit.django_customization_dolphinit",
    # Map tools
    "leaflet",
    # Support libs
    "captcha",
    "django_simple_notifier.django_simple_notifier",
    # OSCAR ECOMMERCE
    "oscar.config.Shop",
    "oscar.apps.analytics.apps.AnalyticsConfig",
    "oscar.apps.checkout.apps.CheckoutConfig",
    "oscar.apps.address.apps.AddressConfig",
    "oscar.apps.shipping.apps.ShippingConfig",
    "oscar.apps.catalogue.apps.CatalogueConfig",
    "oscar.apps.catalogue.reviews.apps.CatalogueReviewsConfig",
    "oscar.apps.communication.apps.CommunicationConfig",
    "oscar.apps.partner.apps.PartnerConfig",
    "oscar.apps.basket.apps.BasketConfig",
    "oscar.apps.payment.apps.PaymentConfig",
    "oscar.apps.offer.apps.OfferConfig",
    "oscar.apps.order.apps.OrderConfig",
    "oscar.apps.customer.apps.CustomerConfig",
    "oscar.apps.search.apps.SearchConfig",
    "oscar.apps.voucher.apps.VoucherConfig",
    "oscar.apps.wishlists.apps.WishlistsConfig",
    "oscar.apps.dashboard.apps.DashboardConfig",
    "oscar.apps.dashboard.reports.apps.ReportsDashboardConfig",
    "oscar.apps.dashboard.users.apps.UsersDashboardConfig",
    "oscar.apps.dashboard.orders.apps.OrdersDashboardConfig",
    "oscar.apps.dashboard.catalogue.apps.CatalogueDashboardConfig",
    "oscar.apps.dashboard.offers.apps.OffersDashboardConfig",
    "oscar.apps.dashboard.partners.apps.PartnersDashboardConfig",
    "oscar.apps.dashboard.pages.apps.PagesDashboardConfig",
    "oscar.apps.dashboard.ranges.apps.RangesDashboardConfig",
    "oscar.apps.dashboard.reviews.apps.ReviewsDashboardConfig",
    "oscar.apps.dashboard.vouchers.apps.VouchersDashboardConfig",
    "oscar.apps.dashboard.communications.apps.CommunicationsDashboardConfig",
    "oscar.apps.dashboard.shipping.apps.ShippingDashboardConfig",
    # OSCAR API
    "oscarapi",
    # 3rd-party apps that oscar depends on
    "widget_tweaks",
    "haystack",
    "treebeard",
    "sorl.thumbnail",  # Default thumbnail backend, can be replaced
    "django_tables2",
]

# Dynamic loading of MDAT modules
for name in os.listdir(PROJECT_ROOT + "/.."):
    if os.path.isdir(name) and name.startswith("django_mdat_"):
        module_name = name + "." + name
        INSTALLED_APPS.append(module_name)

# Dynamic loading of E-COMMERCE modules
for name in os.listdir(PROJECT_ROOT + "/.."):
    if os.path.isdir(name) and name.startswith("django_ecommerce_shop_"):
        module_name = name + "." + name
        INSTALLED_APPS.append(module_name)

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.gzip.GZipMiddleware",
    "django_minify_html.middleware.MinifyHtmlMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "user_sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "x_forwarded_for.middleware.XForwardedForMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.contrib.sites.middleware.CurrentSiteMiddleware",
    "oscar.apps.basket.middleware.BasketMiddleware",
    "django.contrib.flatpages.middleware.FlatpageFallbackMiddleware",
]

AUTHENTICATION_BACKENDS = ("django_python3_ldap.auth.LDAPBackend",)

STATICFILES_STORAGE = "whitenoise.storage.CompressedStaticFilesStorage"

# Account Settings
LOGIN_URL = "/account/login/"
LOGIN_REDIRECT_URL = "/author/dashboard/"
LOGOUT_REDIRECT_URL = "/account/logout/"

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

ROOT_URLCONF = "django_ecommerce_shop.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.request",
                "oscar.apps.search.context_processors.search_form",
                "oscar.apps.checkout.context_processors.checkout",
                "oscar.apps.communication.notifications.context_processors.notifications",
                "oscar.core.context_processors.metadata",
                "django_cookiebot.django_cookiebot.context_processors.cookiebot_js",
            ],
        },
    },
]

DJANGO_WYSIWYG_FLAVOR = "ckeditor"

LANGUAGES = (
    ("en", _("English")),
    ("de", _("German")),
)

PARLER_LANGUAGES = {
    1: (
        # Default SITE_ID, all languages
        {
            "code": "en",
        },
        {
            "code": "de",
        },
    ),
    "default": {
        # This is applied to each entry in this setting:
        "hide_untranslated": False,
        "hide_untranslated_menu_items": False,
    },
}

# DEEPL
DEEPL_API_KEY = config("DEEPL_API_KEY", default="secret", cast=str)

# Robots file
ROBOTS_USE_SITEMAP = False
ROBOTS_SITEMAP_URLS = [
    SITEMAP_PREFIX + "/" + lang[0] + "/sitemap.xml" for lang in LANGUAGES
]

# Celery
CELERY_RESULT_BACKEND = "django-db"
DJANGO_CELERY_BEAT_TZ_AWARE = False
CELERY_BROKER_URL = config("CELERY_BROKER_URL", default="amqp://", cast=str)
CELERY_TASK_DEFAULT_QUEUE = "django_ecommerce_shop"

WSGI_APPLICATION = "django_ecommerce_shop.wsgi.application"

# DEFAULT_FILE_STORAGE = "db_file_storage.storage.DatabaseFileStorage"

# Sentry
SENTRY_DSN = config("SENTRY_DSN", default="sentry_dsn", cast=str)
if not DEBUG and not SENTRY_DSN == "sentry_dsn":
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        send_default_pii=True,
        traces_sample_rate=0.1,
        integrations=[DjangoIntegration(), CeleryIntegration()],
    )

# Database
# https://docs.djangoproject.com/en/stable/ref/settings/#databases

# MAIN DATABASE
MAIN_DATABASE_NAME = config("MAIN_DATABASE_NAME", default="maindb", cast=str)
MAIN_DATABASE_USER = config("MAIN_DATABASE_USER", default="maindb", cast=str)
MAIN_DATABASE_PASSWD = config("MAIN_DATABASE_PASSWD", default="secret", cast=str)
MAIN_DATABASE_HOST = config("MAIN_DATABASE_HOST", default="127.0.0.1", cast=str)
MAIN_DATABASE_PORT = config("MAIN_DATABASE_PORT", default="3306", cast=str)

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": MAIN_DATABASE_NAME,
        "USER": MAIN_DATABASE_USER,
        "PASSWORD": MAIN_DATABASE_PASSWD,
        "HOST": MAIN_DATABASE_HOST,
        "PORT": MAIN_DATABASE_PORT,
        "OPTIONS": {"init_command": "SET sql_mode='STRICT_TRANS_TABLES'"},
    },
}

HAYSTACK_CONNECTIONS = {
    "default": {
        "ENGINE": "haystack.backends.simple_backend.SimpleEngine",
    },
}

# Password validation
# https://docs.djangoproject.com/en/stable/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

THUMBNAIL_HIGH_RESOLUTION = True

CKEDITOR_UPLOAD_PATH = "uploads/"

CKEDITOR_IMAGE_BACKEND = "pillow"

# LDAP server type
LDAP_SERVER_TYPE = config("LDAP_SERVER_TYPE", default="OpenLDAP", cast=str)

# The URL of the LDAP server.
LDAP_AUTH_URL = config("LDAP_AUTH_URL", default="ldaps://localhost:636", cast=str)

# Initiate TLS on connection.
LDAP_AUTH_USE_TLS = config("LDAP_AUTH_USE_TLS", default=True, cast=bool)

# The LDAP search base for looking up users.
LDAP_AUTH_SEARCH_BASE = config(
    "LDAP_AUTH_SEARCH_BASE", default="ou=people,dc=example,dc=com", cast=str
)

# The LDAP class that represents a user.
LDAP_AUTH_OBJECT_CLASS = config(
    "LDAP_AUTH_OBJECT_CLASS", default="inetOrgPerson", cast=str
)

# User model fields mapped to the LDAP
# attributes that represent them.
if LDAP_SERVER_TYPE == "ActiveDirectory":
    LDAP_AUTH_USER_FIELDS = {
        "username": "userPrincipalName",
        "first_name": "givenName",
        "last_name": "sn",
        "email": "mail",
    }
elif LDAP_SERVER_TYPE == "OpenLDAP":
    LDAP_AUTH_USER_FIELDS = {
        "username": "uid",
        "first_name": "givenName",
        "last_name": "sn",
        "email": "mail",
    }
else:
    LDAP_AUTH_USER_FIELDS = {
        "username": "uid",
        "first_name": "givenName",
        "last_name": "sn",
        "email": "mail",
    }

# A tuple of django model fields used to uniquely identify a user.
LDAP_AUTH_USER_LOOKUP_FIELDS = ("username",)

# Path to a callable that takes a dict of {model_field_name: value},
# returning a dict of clean model data.
# Use this to customize how data loaded from LDAP is saved to the User model.
LDAP_AUTH_CLEAN_USER_DATA = "django_python3_ldap.utils.clean_user_data"

# Path to a callable that takes a user model and a dict of {ldap_field_name: [value]},
# and saves any additional user relationships based on the LDAP data.
# Use this to customize how data loaded from LDAP is saved to User model relations.
# For customizing non-related User model fields, use LDAP_AUTH_CLEAN_USER_DATA.
LDAP_AUTH_SYNC_USER_RELATIONS = "django_session_ldap_attributes.django_session_ldap_attributes.ldap_func.sync_user_relations"

# Path to a callable that takes a dict of {ldap_field_name: value},
# returning a list of [ldap_search_filter]. The search filters will then be AND'd
# together when creating the final search filter.
LDAP_AUTH_FORMAT_SEARCH_FILTERS = "django_python3_ldap.utils.format_search_filters"

# Path to a callable that takes a dict of {model_field_name: value}, and returns
# a string of the username to bind to the LDAP server.
# Use this to support different types of LDAP server.
if LDAP_SERVER_TYPE == "ActiveDirectory":
    LDAP_AUTH_FORMAT_USERNAME = (
        "django_python3_ldap.utils.format_username_active_directory_principal"
    )
elif LDAP_SERVER_TYPE == "OpenLDAP":
    LDAP_AUTH_FORMAT_USERNAME = "django_python3_ldap.utils.format_username_openldap"
else:
    LDAP_AUTH_FORMAT_USERNAME = "django_python3_ldap.utils.format_username_openldap"

# Sets the login domain for Active Directory users.
LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN = config(
    "LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN", default=None, cast=str
)

if LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN == "None":
    LDAP_AUTH_ACTIVE_DIRECTORY_DOMAIN = None

# The LDAP username and password of a user for querying the LDAP database for user
# details. If None, then the authenticated user will be used for querying, and
# the `ldap_sync_users` command will perform an anonymous query.
LDAP_AUTH_CONNECTION_USERNAME = config(
    "LDAP_AUTH_CONNECTION_USERNAME", default=None, cast=str
)
LDAP_AUTH_CONNECTION_PASSWORD = config(
    "LDAP_AUTH_CONNECTION_PASSWORD", default=None, cast=str
)

# Set connection/receive timeouts (in seconds) on the underlying `ldap3` library.
LDAP_AUTH_CONNECT_TIMEOUT = None
LDAP_AUTH_RECEIVE_TIMEOUT = None

# ZAMMAD API
ZAMMAD_URL = config("ZAMMAD_URL", default="https://care.example.com/", cast=str)
ZAMMAD_USER = config("ZAMMAD_USER", default="user", cast=str)
ZAMMAD_PASSWD = config("ZAMMAD_PASSWD", default="secret", cast=str)

# COOKIE CONSENT
COOKIEBOT_ID = config("COOKIEBOT_ID", default="xxxxx", cast=str)

# Analytics
FACEBOOK_PIXEL_ID = config("FACEBOOK_PIXEL_ID", default="xxxxx", cast=str)

GOOGLE_ANALYTICS_GTAG_PROPERTY_ID = config(
    "GOOGLE_ANALYTICS_GTAG_PROPERTY_ID", default="12345678", cast=str
)
GOOGLE_ANALYTICS_JS_PROPERTY_ID = config(
    "GOOGLE_ANALYTICS_JS_PROPERTY_ID", default="12345678", cast=str
)
GOOGLE_ANALYTICS_DISPLAY_ADVERTISING = config(
    "GOOGLE_ANALYTICS_DISPLAY_ADVERTISING", default=True, cast=bool
)
GOOGLE_ANALYTICS_ANONYMIZE_IP = config(
    "GOOGLE_ANALYTICS_ANONYMIZE_IP", default=True, cast=bool
)

MATOMO_DOMAIN_PATH = config(
    "MATOMO_DOMAIN_PATH", default="matomo.example.com", cast=str
)
MATOMO_SITE_ID = config("MATOMO_SITE_ID", default="1", cast=str)

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = config("LANGUAGE_CODE", default="en", cast=str)

PARLER_DEFAULT_LANGUAGE_CODE = LANGUAGE_CODE

TIME_ZONE = config("TIME_ZONE", default="UTC", cast=str)

USE_I18N = True

USE_L10N = True

USE_TZ = True

# CAPTCHA
CAPTCHA_NOISE_FUNCTIONS = ("captcha.helpers.noise_null",)
RECAPTCHA_PUBLIC_KEY = config("RECAPTCHA_PUBLIC_KEY", default="secret", cast=str)
RECAPTCHA_PRIVATE_KEY = config("RECAPTCHA_PRIVATE_KEY", default="secret", cast=str)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
